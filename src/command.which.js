'use strict';

const GitDownload = require('git-download');
const path = require('path');
const os = require('os');
const fs = require('fs-extra');

function gitDownload(downloadOptions) {
	return new Promise((resolve, reject) => GitDownload(
		Object.assign({
			tmpDir: os.tmpdir()
		}, downloadOptions),
		(err, data) => err ? reject(err) : resolve(data))
	);
}

function getTagsForVersion (releaseTag) {
	var tmpDir = path.resolve(os.tmpdir(), 'fontoxml-release-' + (new Date().getTime()));

	return gitDownload({
		source: `ssh://git@bitbucket.org/liones/fontoxml-release.git`,
		dest: tmpDir,
		branch: releaseTag
	})
		.then(() => {
			var it = null;
			try {
				it = fs.readJsonSync(path.join(tmpDir, 'fonto-manifest.json')).dependencies
			} catch (e) {}

			if(!it)
				try {
					var deps = fs.readJsonSync(path.join(tmpDir, 'bower.json')).dependencies;
					return Object.keys(deps).reduce((clean, name) => {
						clean[name] = deps[name].substr(deps[name].indexOf('#') + 1);
						return clean;
					}, {})
				} catch (e) {}

			fs.removeSync(tmpDir);

			if (it) return it;

			throw new Error('Could not extract package tags from fontoxml-release#' + releaseTag);
		});
}

module.exports = fotno => {
	function whichCommand(req, res) {
		const bowerDependencies = {},
			sdkVersion = req.options.sdk || req.fdt.editorRepository.sdkVersion;

		try {
			Object.assign(bowerDependencies, require(path.resolve(req.fdt.editorRepository.path, 'bower.json')).dependencies);
		} catch (e) {
			// No-op
		}

		res.caption(`fotno which`);

		res.debug(`Downloading release info for SDK ${sdkVersion}...`);

		return getTagsForVersion(sdkVersion)
			.then(resolvedPackageTags => {
				let allDeps = {};

				Object.keys(resolvedPackageTags).forEach(depName => {
					allDeps[depName] = [resolvedPackageTags[depName]];
				});

				Object.keys(bowerDependencies).forEach(depName => {
					let depVersion = bowerDependencies[depName].substr(bowerDependencies[depName].indexOf('#') + 1) + ' (bower)';
					allDeps[depName] = allDeps[depName]
						? allDeps[depName].concat([depVersion])
						: [depVersion];
				});


				req.parameters.package && res.debug(`Filtering for packages containing "${req.parameters.package}".`);
				let filteredResults = Object.keys(allDeps)
					.filter(depName => req.parameters.package ? depName.indexOf(req.parameters.package) >= 0 : true)
					.map(depName => [depName, allDeps[depName].join(', ')]);

				if(filteredResults.length)
					res.properties(filteredResults);
				else
					res.notice(`No packages matching "${req.parameters.package}".`);
			});
	}

	fotno.registerCommand('which', whichCommand)
		.setDescription('Queries which package version *should* be installed according to the SDK or bower.json')
		.addOption('sdk', 's', 'An SDK version (defaults to application SDK version)', false)
		.addParameter('package', 'A dependency name or a part thereof', false)
		.addExample(`fotno which families`, `Give the version number of fontoxml-families (and possibly others)`)
		.addExample(`fotno which --sdk 6.0.0`, `Give the version numbers of everything in SDK 6.0.0`);
};
